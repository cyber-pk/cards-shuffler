package com.playtech.live.card.shuffle;

import com.playtech.live.card.shuffle.model.Card;

import java.util.List;

public sealed interface CardsShuffler permits CardsShufflerWithDifferentNeighborsBySuit {

    List<Card> shuffle(List<Card> deck);

}