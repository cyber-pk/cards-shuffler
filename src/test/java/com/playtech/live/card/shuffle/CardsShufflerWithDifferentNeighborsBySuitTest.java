package com.playtech.live.card.shuffle;

import static com.playtech.live.card.shuffle.model.Card.C3;
import static com.playtech.live.card.shuffle.model.Card.C4;
import static com.playtech.live.card.shuffle.model.Card.C5;
import static com.playtech.live.card.shuffle.model.Card.D5;
import static com.playtech.live.card.shuffle.model.Card.D7;
import static com.playtech.live.card.shuffle.model.Card.SA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.parallel.ExecutionMode.CONCURRENT;

import com.playtech.live.card.shuffle.model.Card;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

// Do we have enough tests to ensure correctness of CardsShufflerWithDifferentNeighborsBySuit?
@Execution(CONCURRENT)
class CardsShufflerWithDifferentNeighborsBySuitTest {

    @Test
    void shuffle_EmptyDeck() {
        var shuffler = new CardsShufflerWithDifferentNeighborsBySuit(ThreadLocalRandom.current());

        List<Card> deck = List.of();
        List<Card> shuffledDeck = shuffler.shuffle(deck);

        assertTrue(shuffledDeck.isEmpty());
    }

    @Test
    void shuffle_SingleCard() {
        var shuffler = new CardsShufflerWithDifferentNeighborsBySuit(ThreadLocalRandom.current());

        List<Card> deck = List.of(C3);
        List<Card> shuffledDeck = shuffler.shuffle(deck);

        assertContainsAllInAnyOrder(deck, shuffledDeck);
    }

    @Test
    void shuffle_With2SameCardsBySuit() {
        var shuffler = new CardsShufflerWithDifferentNeighborsBySuit(ThreadLocalRandom.current());

        List<Card> deck = List.of(C3, C4);

        assertThrows(IllegalArgumentException.class, () -> shuffler.shuffle(deck));
    }

    @RepeatedTest(1000)
    void shuffle() {
        var shuffler = new CardsShufflerWithDifferentNeighborsBySuit(ThreadLocalRandom.current());

        List<Card> deck = List.of(D5, C3, C4, C5, SA, D7);
        List<Card> shuffledDeck = shuffler.shuffle(deck);

        assertContainsAllInAnyOrder(deck, shuffledDeck);
        assertNeighborCardsHaveDifferentSuits(shuffledDeck);
    }

    @RepeatedTest(1000)
    void shuffle_OnWholeDeck() {
        var shuffler = new CardsShufflerWithDifferentNeighborsBySuit(ThreadLocalRandom.current());

        List<Card> deck = randomlyShuffledDeck();
        List<Card> shuffledDeck = shuffler.shuffle(deck);

        assertContainsAllInAnyOrder(deck, shuffledDeck);
        assertNeighborCardsHaveDifferentSuits(shuffledDeck);
    }

    private static List<Card> randomlyShuffledDeck() {
        List<Card> deck = new ArrayList<>(List.of(Card.values()));
        Collections.shuffle(deck, ThreadLocalRandom.current());
        return deck;
    }

    private static void assertContainsAllInAnyOrder(List<Card> expected, List<Card> actual) {
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
    }

    private static void assertNeighborCardsHaveDifferentSuits(List<Card> cards) {
        checkArgument(cards.size() > 1, "At least 2 cards are needed to check the suits of neighbor cards!");

        for (int i = 1; i < cards.size(); i++) {
            assertNotEquals(cards.get(i).suit(), cards.get(i - 1).suit(), "Same suit on neighbor positions: " + i + " and " + (i - 1));
        }
    }

    @SuppressWarnings("SameParameterValue")
    private static void checkArgument(boolean exp, String msg) {
        if (!exp) {
            throw new IllegalArgumentException(msg);
        }
    }
}